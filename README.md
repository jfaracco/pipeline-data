# pipeline-data

Configuration files used with https://gitlab.com/cki-project/pipeline-trigger


# New tree onboarding

* If you can, just submit a PR! We'll help you with specific fields if you are
  not sure.
* If you don't want to submit a PR, you can submit and issue or contact us
  directly with the repo/email information and we can do it for you.

## Onboarding notes for CKI team

* If this is a new tree, add it to `git-cache-updater`! If you don't do this the
  repo cloning will crash. There's an internal JIRA ticket opened to handle this
  automatically without manual intervention.
* Make sure the CKI branch from configuration exists and links to the correct
  pipeline definition tree.
